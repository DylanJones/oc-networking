local component = require"component"
local coroutine = require"coroutine"
local event = require"event"
local os = require"os"



-- ip entry = "<the ip>": {"expiry": os.time + IP_TIMEOUT, "ip": "<the ip>", "mac": "<the mac>"}
local ipTable = {}
-- device entry: {"ip": "<the ip if it has one>", "netmask": "<the subnet mask>","mac": "<the mac", "send", function send(destMac, packet) ... end, "interface": interface raw device thingy}
local devices = {}
-- routes entry: {"netmask": "<the netmask>", "subnet": "<FIRST ip in that subnet>", "gateway": "either the IP of the gateway or the MAC of the interface that owns that route"}
local routes = {}
local handlers = {}
local IP_TIMEOUT = 43200
local CONFIG_FILE = "/etc/networking.cfg"
local DEBUGGING = true

-- Load config
local config = {}
loadfile(CONFIG_FILE, 't', config)()

function dbg(...)
	if DEBUGGING then
		print(...)
	end
end


function initDevices()
	-- initalize the devices array
	
	-- Deal with modems
	dbg("trying init")
	event.listen("modem_message", onPacket)
	-- Populate devices with all network interfaces
	for addr in component.list('modem', true) do
		local modem = component.proxy(addr)
		devices[#devices+1] = {mac=modem.address, interface=modem, send=function(destMac, ...)
			modem.send(destMac, 1, ...)
		end, broadcast=function(...)
			modem.broadcast(1, ...)
		end}
	end
	for addr in component.list('tunnel', true) do
		local tunnel = component.proxy(addr)
		devices[#devices+1] = {mac=tunnel.address, interface=tunnel, send=function(destMac, ...)
			tunnel.send(...)
		end, broadcast=function(...)
			tunnel.send(...)
		end}
	end
	-- Get the configuration for all defined interfaces 
	for i,device in pairs(devices) do
		local devConfig = config.interfaces[device.mac]
		if devConfig ~= nil then
			dbg("Parsing configuration data for interface "..device.mac)
			-- open the main communication port for listening
			if device.interface.type == 'modem' then
				device.interface.open(1)
				-- set communication strength if wifi
				if device.interface.isWireless() then
					device.interface.setStrength(5000)
				end
			end
			if devConfig.dhcp then
				-- do dhcp stuff
			else
				-- we have a static ip
				device.ip = devConfig.ip
				device.netmask = devConfig.netmask
				-- add the physical route to the routing table
				local netaddr = numToDotted(dottedToNum(device.netmask) & dottedToNum(device.ip))
				routes[#routes+1] = {netmask=device.netmask, subnet=netaddr, gateway=device.mac}
			end
		else
			dbg("No configuration found for interface "..device.modem.address)
		end
	end

	for i,route in pairs(config.routes) do
		routes[#routes+1] = route
	end
end

------------- HELPERS --------------

function lookupDeviceByMac(mac)
	for i,device in pairs(devices) do
		if device.mac == mac then
			return device
		end
	end
end

function lookupDeviceByIp(ip)
	for i,device in pairs(devices) do
		if device.ip == ip then
			return device
		end
	end
end

-- Convert an IP in dotted octed form into a single integer
function dottedToNum(str)
	local num = 0
	local i = 0
	for piece in str:gmatch("([^.]+)") do
		num = (num << 8) | (tonumber(piece) << i * 8)
	end
	return num
end

-- Convert an integer into a 4-octet dotted number
function numToDotted(num)
	local str = ""
	for i = 3,0,-1 do
		local octet = (num >> (i * 8)) & 0xFF
		str = str..octet.."."
	end
	return str:sub(1,str:len()-1)
end

-- Get the adapter that is in the same subnet as the given IP. Returns nil if not found
function lookupAdapterBySubnet(subnetIp)
	local searchIp = dottedToNum(subnetIp)
	for i,device in pairs(devices) do 
		if device.ip ~= nil then
			local mask = dottedToNum(device.netmask)
			local net = dottedToNum(device.ip)
			if net & mask == searchIp & mask then
				return device
			end
		end
	end
end

-- Return the first route found to the given IP address.  Returns nil if no route found.
function lookupRouteToIp(ip)
	local searchIp = dottedToNum(ip)
	for i,route in pairs(routes) do 
		local mask = dottedToNum(route.netmask)
		local net = dottedToNum(route.subnet)
		if net & mask == searchIp & mask then
			return route
		end
	end
end

-- Check if a string is an ip address or not.
function isIp(str)
	-- check for format 1.11.111.111 for ipv4
	local chunks = {str:match("(%d+)%.(%d+)%.(%d+)%.(%d+)")}
	if (#chunks == 4) then
		for _,v in pairs(chunks) do
			if (tonumber(v) < 0 or tonumber(v) > 255) then
				return false
			end
		end
		return true
	end
	return false
	-- bad - just checks if there are 3 periods
	--return select(2, str:gsub("%.", "")) == 3
end

--------------- HANDLERS -------------

-- "packet" varible format:
-- _, local_address, remote_address, port (the hardware useless one), distance(useless), packet_type, data...

-- ARP format:
-- ["ARP", <wanted_IP>]

-- Handle packets in general
function onPacket(...)
	-- process it
	local arg = {...}
	dbg("received packet from "..arg[3])
	if #arg < 6 then
		dbg("Packet too small")
		return
	end
	local packetType = arg[6]
	if handlers[packetType] == nil then
		dbg("Packet type "..packetType.." is unknown!")
		return
	end
	dbg("Received packet: ", ...)
	dbg("Attempting to dispatch packet of type "..packetType)
	dbg(pcall(handlers[packetType], arg))
	-- handlers[packetType](arg[7:])
end

-- Handle any incoming arp requests
function handlers.ARP_REQUEST(packet)
	dbg("Received ARP request from ".. packet[3])
	if #packet ~= 7 then
		dbg("ARP request packet wrong size!")
		return
	end

	local wantedIp = packet[7]
	dbg(wantedIp)
	dbg(lookupDeviceByMac(packet[2]).ip)
	if lookupDeviceByMac(packet[2]).ip == wantedIp then
		dbg("We have the wanted address, reply with our MAC")
		lookupDeviceByMac(packet[2]).send(packet[3], "ARP_REPLY", wantedIp)
	end
end

-- Handle any ARP replies
function handlers.ARP_REPLY(packet)
	-- Check that IP isn't already in the table
	if #packet ~= 7 then
		dbg("ARP reply packet wrong size!")
		return
	end
	ipTable[packet[7]] = {expiry=os.time() + IP_TIMEOUT, ip=packet[7], mac=packet[3]}
	dbg("Received ARP reply from "..packet[3].." for IP "..packet[7])
end

-- Handle data packets - this includes forwarding them to their destinations
function handlers.DATA(packet)
	if #packet < 7 then
		dbg("Data packet wrong size!")
		return
	end
	
	local destIp = packet[7]
	if lookupDeviceByIp(destIp) ~= nil then -- it was intended for us
		-- client code, idk?
		dbg("Data packet was for us!")
	else
		-- not for us, forward if we can
		local route = lookupRouteToIp(destIp)
		if route ~= nil then
			-- forward it
			sendRaw(destIp, table.unpack(packet, 6, #packet))
		else
			-- can't route to destination - TODO figure out how to indicate failure
		end
	end
end


------------ CLIENT THINGIES ---------

function arpLookupMac(ip)
	local entry = ipTable[ip]
	dbg("Looking up "..ip.." in ARP table")
	if entry == nil or entry.expiry < os.time() then
		-- update entry if it is bad or expired
		dbg("Entry expired or invalid -- attempting to resolve via ARP")
		lookupAdapterBySubnet(ip).broadcast("ARP_REQUEST", ip)
		while ipTable[ip] == nil or ipTable[ip].expiry < os.time() do
			os.sleep(0.1)
		end
	end
	entry = ipTable[ip]
	return entry.mac
end

function send(destIp, ...)
	-- TODO figure out our source IP and add it here
	local srcIp = '192.168.1.2'
	sendRaw(destIp, "DATA", destIp, srcIp, ...)
end

function sendRaw(destIp, ...)
	dbg("Attempting to send packet to "..destIp.." with data", ...)
	-- if dest in one of our local subnets then
	-- 	do an arp lookup and send via local
	-- else
	-- 	try to send it via any gateways
	-- end
	local srcDevice = lookupAdapterBySubnet(destIp)
	local route = lookupRouteToIp(destIp)
	if route == nil then
		print("No route to destination found!")
		print(nil + 2)
	end
	if isIp(route.gateway) then
		dbg("Route requires a gateway hop - recuring with gateway")
		sendRaw(route.gateway, ...)
	else
		dbg("Direct route found through local interface.")
		local destMac = arpLookupMac(destIp)
		dbg("Sending packet to "..destMac)
		srcDevice.send(destMac, ...)
	end
	
end


initDevices()
return {send=send, handlers=handlers, devices=devices, lookupDeviceByMac=lookupDeviceByMac, 
	lookupDeviceByIp=lookupDeviceByIp, ipTable=ipTable, routes=routes, dottedToNum=dottedToNum,
	numToDotted=numToDotted, sendRaw=sendRaw, isIp=isIp}
